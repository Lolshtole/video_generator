package ffive.video.videocreator.videogen;

public class VideoParams {

    public String history;
    public String videoPath;
    public int[] original;
    public int[] palette;
    public int originalWidth;
    public int originalHeight;

    public int secondsPaint;
    public int secondsShow;

}
