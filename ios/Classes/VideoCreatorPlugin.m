#import "VideoCreatorPlugin.h"
#import <video_creator/video_creator-Swift.h>

@implementation VideoCreatorPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftVideoCreatorPlugin registerWithRegistrar:registrar];
}
@end
